package com.dengbw.homecenter.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author dengbw
 * 2022年07月26日 14:40
 */
class UploadFileServiceTest {

    @Test
    void getFormat() {
        String n1 = "123.a";
        assertEquals("a", UploadFileService.getFormat(n1));
    }
}

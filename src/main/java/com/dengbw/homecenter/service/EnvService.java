package com.dengbw.homecenter.service;

import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.EnvMapper;
import com.dengbw.homecenter.model.db.Env;
import org.springframework.stereotype.Service;

/**
 * @author dengbw
 * 2022年07月13日 14:34
 */
@Service
public class EnvService extends MyService<EnvMapper, Env> {
}

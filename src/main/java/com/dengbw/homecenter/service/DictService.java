package com.dengbw.homecenter.service;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.basic.exception.BizException;
import com.dengbw.homecenter.basic.exception.ErrorEnum;
import com.dengbw.homecenter.mapper.DictMapper;
import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.model.db.Dict;
import com.dengbw.homecenter.model.vo.DictVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dengbw
 * 2022年08月03日 13:32
 */
@Service
public class DictService extends MyService<DictMapper, Dict> {
    public List<DictVo> findDictsOf(String keyword) {
        List<Dict> dictList;
        if (StringUtils.isBlank(keyword)) {
            dictList = list();
        } else {
            List<Dict> related = list(v -> v.like(Dict::getShowing, keyword)
                    .or(a -> v.like(Dict::getCategory, keyword))
                    .or(b -> v.like(Dict::getValue, keyword)));
            dictList = list(v -> v.in(Dict::getId, related.stream().map(Dict::getId).collect(Collectors.toSet())).orderByAsc(Dict::getCategory).orderByAsc(Dict::getShowing).orderByAsc(Dict::getValue));
        }
        Map<String, List<Dict>> dictMap = dictList.stream().collect(Collectors.groupingBy(Dict::getCategory));
        return dictMap.values().stream().map(DictVo::new).toList();
    }

    public synchronized Result<String> createDict(Dict dict) {
        if (dict == null) {
            throw new BizException(ErrorEnum.EMPTY_INPUT);
        }
        if (StringUtils.isBlank(dict.getCategory())) {
            throw new BizException(ErrorEnum.EMPTY_CATEGORY);
        }
        if (StringUtils.isBlank(dict.getValue())) {
            throw new BizException(ErrorEnum.EMPTY_VALUE);
        }
        List<Dict> dictList = list(v -> v.eq(Dict::getCategory, dict.getCategory()).eq(Dict::getValue, dict.getValue()));
        if (CollectionUtils.isEmpty(dictList)) {
            save(dict);
            return Result.ok();
        } else {
            throw new BizException(ErrorEnum.CREATE_DUPLICATE);
        }
    }

    public void removeDict(String id) {
        if (StringUtils.isBlank(id)) {
            return;
        }
        removeById(id);
    }
}

package com.dengbw.homecenter.service;

import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.BomRelationMapper;
import com.dengbw.homecenter.model.db.BomRelation;
import org.springframework.stereotype.Service;

/**
 * @author dengbw
 * 2022年07月11日 13:46
 */
@Service
public class BomRelationService extends MyService<BomRelationMapper, BomRelation> {
}

package com.dengbw.homecenter.service;

import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.UploadFileMapper;
import com.dengbw.homecenter.model.db.UploadFile;
import com.dengbw.homecenter.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author dengbw
 * 2022年07月19日 16:17
 */
@Service
public class UploadFileService extends MyService<UploadFileMapper, UploadFile> {

    @Value("${file.base}")
    private String fileBase;

    public synchronized String receiveFile(MultipartFile file) {
        new File(fileBase).mkdirs();
        UploadFile uploadFile = new UploadFile();
        uploadFile.setSize(file.getSize());
        uploadFile.setOriginName(file.getOriginalFilename());
        uploadFile.setFormat(getFormat(uploadFile.getOriginName()));
        if (exist(fileBase + File.separator + file.getOriginalFilename())) {
            String name = uploadFile.getOriginName().replace("\\." + uploadFile.getFormat(), "");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS");
            uploadFile.setPath(fileBase + File.separator + name + "_" + formatter.format(LocalDateTime.now()) + "." + uploadFile.getFormat());
        } else {
            uploadFile.setPath(fileBase + File.separator + file.getOriginalFilename());
        }
        saveToDisk(file, uploadFile.getPath());
        uploadFile.setCreateTime(LocalDateTime.now());
        save(uploadFile);
        return uploadFile.getId();
    }

    private boolean exist(String fullPath) {
        File file = new File(fullPath);
        return file.exists();
    }

    public static String getFormat(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    private void saveToDisk(MultipartFile multiFile, String path) {
        try(InputStream is = multiFile.getInputStream()) {
            FileUtils.saveTo(is, path);
        } catch (IOException e) {
            log.error("文件保存出错: " + e.getMessage());
        }
    }
}

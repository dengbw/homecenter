package com.dengbw.homecenter.service;

import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.LogMapper;
import com.dengbw.homecenter.model.db.Log;
import org.springframework.stereotype.Service;

/**
 * @author dengbw
 * 2022年07月14日 14:23
 */
@Service
public class LogService extends MyService<LogMapper, Log> {
}

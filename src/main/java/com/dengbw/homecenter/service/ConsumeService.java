package com.dengbw.homecenter.service;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.ConsumeMapper;
import com.dengbw.homecenter.model.db.Consume;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author dengbw
 * 2022年07月22日 09:31
 */
@Service
public class ConsumeService extends MyService<ConsumeMapper, Consume> {
    public BigDecimal queryTotalConsume() {
        List<Consume> consumes = list(v -> v.eq(Consume::getCanceled, false));
        return calculateSum(consumes);
    }

    public BigDecimal calculateSum(Collection<Consume> consumes) {
        AtomicReference<BigDecimal> sum = new AtomicReference<>(new BigDecimal("0"));
        consumes.forEach(v -> sum.set(sum.get().add(v.getMoney())));
        return sum.get();
    }

    public List<Consume> queryBeforeConsumes(LocalDateTime occurTime) {
        return list(v -> v.eq(Consume::getEarlyPaid, false).le(Consume::getOccurTime, occurTime).eq(Consume::getSource, "建设银行装修贷").orderByDesc(Consume::getOccurTime));
    }

    public void cancelRecord(String id) {
        if (Boolean.FALSE.equals(existsId(id))) {
            return;
        }
        update(v -> v.set(Consume::getCanceled, true).eq(Consume::getId, id));
    }

    public void earlyPaidRecord(String id) {
        if (Boolean.FALSE.equals(existsId(id))) {
            return;
        }
        update(v -> v.set(Consume::getEarlyPaid, true).eq(Consume::getId, id));
    }

    private Boolean existsId(String id) {
        List<Consume> consumes = list(v -> v.eq(Consume::getId, id));
        return CollectionUtils.isNotEmpty(consumes);
    }
}

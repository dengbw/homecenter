package com.dengbw.homecenter.service;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.BomItemMapper;
import com.dengbw.homecenter.model.db.BomItem;
import com.dengbw.homecenter.model.db.BomRelation;
import com.dengbw.homecenter.model.vo.BomExcel;
import com.dengbw.homecenter.utils.excel.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author dengbw
 * 2022年07月11日 10:51
 */
@Slf4j
@Service
public class BomItemService extends MyService<BomItemMapper, BomItem> {
    @Autowired
    private BomRelationService bomRelationService;

    public int importFromExcel(String bomId, MultipartFile file) throws Exception {
        List<BomExcel> items = ExcelUtils.readMultipartFile(file, BomExcel.class).stream().filter(v -> StringUtils.isNotBlank(v.getComment())).toList();
        items.forEach(i -> {
            BomItem importItem = new BomItem(i);
            BomItem originItem = getItem(importItem);
            if (originItem == null) {
                save(importItem);
            } else {
                originItem.setQuantityRemained(originItem.getQuantityRemained() + i.getQuantityBought());
                updateById(originItem);
                importItem = originItem;
            }
            updateRelation(bomId, importItem, i);
        });
        return items.size();
    }

    private void updateRelation(String bomId, BomItem importItem, BomExcel bomExcel) {
        if (StringUtils.isBlank(bomId)) {
            return;
        }
        BomRelation relation = new BomRelation(bomId, importItem.getId(), bomExcel.getQuantityRequired(), bomExcel.getDesignator());
        bomRelationService.save(relation);
    }

    public BomItem getItem(BomItem item) {
        List<BomItem> items = list(v -> v
                .eq(StringUtils.isNotBlank(item.getComment()), BomItem::getComment, item.getComment())
                .eq(StringUtils.isNotBlank(item.getDescription()), BomItem::getDescription, item.getDescription())
                .eq(StringUtils.isNotBlank(item.getFootprint()), BomItem::getFootprint, item.getFootprint())
                .eq(StringUtils.isNotBlank(item.getLibRef()), BomItem::getLibRef, item.getLibRef()));
        return CollectionUtils.isEmpty(items) ? null : items.get(0);
    }

    public void boundItemPicture(BomItem item) {
        if (item == null) {
            log.info("Bom item is null, will not bound picture.");
            return;
        }
        if (StringUtils.isBlank(item.getImage())) {
            log.info("Picture url is null, will not bound.");
            return;
        }
        if (StringUtils.isBlank(item.getId())) {
            log.info("Bom item id is null, will not bound picture.");
            return;
        }
        update(v -> v.eq(BomItem::getId, item.getId()).set(BomItem::getImage, item.getImage()));
    }
}

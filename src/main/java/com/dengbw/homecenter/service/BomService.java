package com.dengbw.homecenter.service;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.BomMapper;
import com.dengbw.homecenter.model.db.Bom;
import com.dengbw.homecenter.model.db.BomItem;
import com.dengbw.homecenter.model.db.BomRelation;
import com.dengbw.homecenter.model.vo.BomItemVo;
import com.dengbw.homecenter.model.vo.BomVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author dengbw
 * 2022年07月11日 10:50
 */
@Service
public class BomService extends MyService<BomMapper, Bom> {
    @Autowired
    private BomRelationService relationService;
    @Autowired
    private BomItemService itemService;

    public Collection<BomVo> findBom(String id, String name, String description) {
        List<Bom> bomList = list(v -> v
                .eq(StringUtils.isNotBlank(id), Bom::getId, id)
                .like(StringUtils.isNotBlank(name), Bom::getName, name)
                .like(StringUtils.isNotBlank(description), Bom::getDescription, description)
                .orderByDesc(Bom::getCreateTime));
        if (CollectionUtils.isEmpty(bomList)) {
            return Collections.emptyList();
        }
        Map<String, BomVo> voMap = new HashMap<>(bomList.size());
        bomList.forEach(v -> {
            BomVo vo = new BomVo();
            vo.setId(v.getId());
            vo.setName(v.getName());
            vo.setDescription(v.getDescription());
            voMap.put(vo.getId(), vo);
        });
        List<BomRelation> relationList = relationService.list(v -> v.in(BomRelation::getBomId, bomList.stream().map(Bom::getId).toList()));
        List<BomItem> itemList = itemService.list(v -> v.in(BomItem::getId, relationList.stream().map(BomRelation::getBomItemId).toList()));
        Map<String, List<BomRelation>> relationMap = relationList.stream().collect(Collectors.groupingBy(BomRelation::getBomId));
        Map<String, BomItem> itemMap = itemList.stream().collect(Collectors.toMap(BomItem::getId, Function.identity()));
        relationMap.forEach((k, v) -> {
            BomVo vo = voMap.get(k);
            vo.setItems(v.stream()
                    .map(r -> {
                        BomItem item = itemMap.get(r.getBomItemId());
                        return new BomItemVo(item, r);
                    }).toList());
        });
        return voMap.values();
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BomItem> consume(String id) {
        if (StringUtils.isBlank(id)) {
            return Collections.emptyList();
        }
        List<BomRelation> relations = relationService.list(v -> v.eq(BomRelation::getBomId, id));
        List<BomItem> items = itemService.list(v -> v.in(BomItem::getId, relations.stream().map(BomRelation::getBomItemId).toList()));
        Map<String, BomRelation> relationMap = relations.stream().collect(Collectors.toMap(BomRelation::getBomItemId, Function.identity()));
        List<BomItem> notEnoughItems = items.stream().filter(v -> {
            BomRelation relation = relationMap.get(v.getId());
            return v.getQuantityRemained() == 0 || v.getQuantityRemained().compareTo(relation.getQuantityRequired()) < 0;
        }).toList();
        if (CollectionUtils.isNotEmpty(notEnoughItems)) {
            return notEnoughItems;
        }
        items.forEach(v -> v.setQuantityRemained(v.getQuantityRemained() - relationMap.get(v.getId()).getQuantityRequired()));
        itemService.updateBatchById(items);
        return Collections.emptyList();
    }
}

package com.dengbw.homecenter.service;

import com.dengbw.homecenter.basic.MyService;
import com.dengbw.homecenter.mapper.PayBackMapper;
import com.dengbw.homecenter.model.db.Consume;
import com.dengbw.homecenter.model.db.PayBack;
import com.dengbw.homecenter.model.vo.PaybackVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dengbw
 * 2022年07月22日 09:31
 */
@Slf4j
@Service
public class PayBackService extends MyService<PayBackMapper, PayBack> {

    @Autowired
    private ConsumeService consumeService;
    private static final BigDecimal times = new BigDecimal("96");
    private static final BigDecimal principalRate = new BigDecimal("0.0027");

    public PaybackVo calculateCurrentPayBack() {
        List<Consume> consumes = consumeService.queryBeforeConsumes(LocalDateTime.now());
        PaybackVo vo = new PaybackVo();
        consumes.forEach(v -> {
            BigDecimal principal = v.getMoney().divide(times, 2, RoundingMode.HALF_UP);
            BigDecimal interest = v.getMoney().multiply(principalRate).setScale(2, RoundingMode.HALF_UP);
            vo.setTotal(new BigDecimal(vo.getTotal()).add(principal).add(interest).toString());
            vo.setPrincipal(new BigDecimal(vo.getPrincipal()).add(principal).toString());
            vo.setInterest(new BigDecimal(vo.getInterest()).add(interest).toString());
            log.info(v.getTarget() + "-" + v.getDescription() + ":");
            log.info("\t本金: " + principal + ";");
            log.info("\t利息: " + interest + ";");
        });
        return vo;
    }

    public PaybackVo calculateTotalPayBack() {
        List<PayBack> payBacks = list();
        PaybackVo vo = new PaybackVo();
        payBacks.forEach(v -> {
            vo.setTotal(new BigDecimal(vo.getTotal()).add(v.getTotal()).toString());
            vo.setPrincipal(new BigDecimal(vo.getPrincipal()).add(v.getPrincipal()).toString());
            vo.setInterest(new BigDecimal(vo.getInterest()).add(v.getInterest()).toString());
        });
        return vo;
    }

}

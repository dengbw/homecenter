package com.dengbw.homecenter.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author dengbw
 * 2022年07月14日 14:25
 */
public class IOUtils {

    private IOUtils() {}

    public static void close(Closeable closeable) throws IOException {
        if (null != closeable) {
            closeable.close();
        }
    }

    public static void close(AutoCloseable closeable) throws Exception {
        if (null != closeable) {
            closeable.close();
        }
    }
}

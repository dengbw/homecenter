package com.dengbw.homecenter.utils;

import java.time.LocalDateTime;

/**
 * @author dengbw
 * 2022年07月11日 15:22
 */
public class DateUtils {
    private DateUtils(){}

    public static LocalDateTime getMonthStart() {
        LocalDateTime time = LocalDateTime.now();
        return time.withDayOfMonth(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0)
                .withNano(0);
    }
}

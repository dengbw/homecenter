package com.dengbw.homecenter.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author dengbw
 * 2022年07月14日 14:25
 */
public class ExceptionUtils {
    private ExceptionUtils() {}

    public static String getMessage(Exception e) throws IOException {
        try(StringWriter sw = new StringWriter();PrintWriter pw = new PrintWriter(sw)) {
            // 将出错的栈信息输出到printWriter中
            pw.flush();
            sw.flush();
            return e.getMessage();
        }
    }

    /**
     * 获取堆栈信息
     */
    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }
}

package com.dengbw.homecenter.utils;

import cn.hutool.core.io.FileUtil;

import java.io.*;

/**
 * @author dengbw
 * 2022年07月14日 14:26
 */
public class FileUtils extends FileUtil {
    public static void saveTo(InputStream is, String pathName) throws IOException {
        File file = new File(pathName);
        if (file.exists()) {
            return;
        }
        try(OutputStream os = new FileOutputStream(file)) {
            int bytesRead;
            int len = 8 * 1024;
            byte[] buffer = new byte[len];
            while ((bytesRead = is.read(buffer, 0, len)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.close(is);
        }
    }
}

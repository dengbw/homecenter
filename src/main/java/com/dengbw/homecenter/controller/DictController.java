package com.dengbw.homecenter.controller;

import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.model.db.Dict;
import com.dengbw.homecenter.model.vo.DictVo;
import com.dengbw.homecenter.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dengbw
 * 2022年08月03日 13:32
 */
@Slf4j
@Api(tags = "字典")
@RequestMapping("dict")
@RestController
public class DictController {
    @Autowired
    private DictService dictService;

    @ApiOperation("查询所有字典")
    @GetMapping
    public Result<List<DictVo>> queryDicts(@RequestParam(value = "about", defaultValue = "") String keyword) {
        return Result.ok(dictService.findDictsOf(keyword));
    }

    @ApiOperation("新增字典")
    @PutMapping
    public Result<String> createDict(@RequestBody Dict dict) {
        return dictService.createDict(dict);
    }

    @ApiOperation("删除字典")
    @DeleteMapping
    public Result<String> removeDict(@RequestParam("id") String id) {
        dictService.removeDict(id);
        return Result.ok();
    }
}

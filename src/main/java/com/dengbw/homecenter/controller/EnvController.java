package com.dengbw.homecenter.controller;

import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.model.db.Env;
import com.dengbw.homecenter.service.EnvService;
import com.dengbw.homecenter.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author dengbw
 * 2022年07月13日 14:35
 */
@Api(tags = "环境")
@RestController
@RequestMapping("env")
public class EnvController {

    @Autowired
    private EnvService service;

    @ApiOperation("当前温度")
    @GetMapping("current")
    public Result<Env> current() {
        return Result.ok(service.list(v -> v.orderByDesc(Env::getCreateTime).last(" limit 1")).get(0));
    }

    @ApiOperation("本月数据")
    @GetMapping("current/month")
    public Result<List<Env>> month() {
        return Result.ok(service.list(v -> v.ge(Env::getCreateTime, DateUtils.getMonthStart()).orderByAsc(Env::getCreateTime)));
    }
}

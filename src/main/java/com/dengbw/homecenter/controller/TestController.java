package com.dengbw.homecenter.controller;

import com.dengbw.homecenter.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dengbw
 * 2022年07月11日 09:34
 */
@Slf4j
@RestController
@RequestMapping("test")
public class TestController {
    @GetMapping
    public Result<String> test() {
        return Result.ok();
    }
}

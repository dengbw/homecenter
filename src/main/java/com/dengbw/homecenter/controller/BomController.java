package com.dengbw.homecenter.controller;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.model.db.Bom;
import com.dengbw.homecenter.model.db.BomItem;
import com.dengbw.homecenter.model.vo.BomVo;
import com.dengbw.homecenter.service.BomItemService;
import com.dengbw.homecenter.service.BomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * @author dengbw
 * 2022年07月11日 10:37
 */
@Slf4j
@Api(tags = "Bom与元件库存")
@RestController
@RequestMapping("bom")
public class BomController {

    @Autowired
    private BomService bomService;
    @Autowired
    private BomItemService bomItemService;

    @ApiOperation("查询bom")
    @GetMapping
    public Result<Collection<BomVo>> findBom(@RequestParam(value = "id", defaultValue = "") String id,
                                             @RequestParam(value = "name", defaultValue = "") String name,
                                             @RequestParam(value = "desc", defaultValue = "") String description) {
        return Result.ok(bomService.findBom(id, name, description));
    }

    @ApiOperation("添加BOM")
    @PutMapping
    public Result<String> addBom(@RequestBody Bom bom) {
        bomService.save(bom);
        return Result.ok(bom.getId());
    }

    @ApiOperation("Bom使用")
    @PostMapping("consume")
    public Result<Object> useBom(@RequestParam(value = "id", defaultValue = "") String id) {
        List<BomItem> notEnoughItems = bomService.consume(id);
        if (CollectionUtils.isEmpty(notEnoughItems)) {
            return Result.ok();
        } else {
            return Result.failed("Item not enough!", notEnoughItems);
        }
    }

    @ApiOperation("导入BOM文件")
    @PutMapping("item/import")
    public Result<Integer> importFromExcel(MultipartFile file,
                                           @RequestParam(value = "bom", defaultValue = "") String bomId) throws Exception {
        return Result.ok(bomItemService.importFromExcel(bomId, file));
    }

    @ApiOperation("绑定元件图片")
    @PostMapping ("item/picture/bound")
    public Result<String> boundBomItemPicture(@RequestBody BomItem item) {
        bomItemService.boundItemPicture(item);
        return Result.ok();
    }
}

package com.dengbw.homecenter.controller;

import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.service.UploadFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dengbw
 * 2022年07月26日 14:01
 */
@Slf4j
@Api(tags = "文件")
@RequestMapping("file")
@RestController
public class FileController {
    @Autowired
    private UploadFileService fileService;

    @ApiOperation("上传文件")
    @PostMapping("upload")
    public Result<String> upload(MultipartFile file) {
        return Result.ok(fileService.receiveFile(file));
    }
}

package com.dengbw.homecenter.controller;

import com.dengbw.homecenter.model.Result;
import com.dengbw.homecenter.model.db.Consume;
import com.dengbw.homecenter.model.db.PayBack;
import com.dengbw.homecenter.model.vo.PaybackVo;
import com.dengbw.homecenter.service.ConsumeService;
import com.dengbw.homecenter.service.PayBackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dengbw
 * 2022年07月22日 09:31
 */
@Slf4j
@Api(tags = "装修消费记录")
@RestController
@RequestMapping("consume")
public class ConsumeController {
    @Autowired
    private ConsumeService consumeService;
    @Autowired
    private PayBackService payBackService;

    @ApiOperation(value = "1.查询消费记录", notes = "查询消费记录，不包括退款的消费记录")
    @GetMapping
    public Result<List<Consume>> queryConsume() {
        return Result.ok(consumeService.list(v -> v.eq(Consume::getCanceled, false).orderByAsc(Consume::getOccurTime)));
    }

    @ApiOperation(value = "2.统计花费总金额", notes = "只统计正常还款和提前还款的，不统计退款的")
    @GetMapping("total")
    public Result<BigDecimal> queryTotalConsume() {
        return Result.ok(consumeService.queryTotalConsume());
    }

    @ApiOperation("3.添加消费记录")
    @PostMapping
    public Result<Boolean> addConsumeRecord(@RequestBody Consume consume) {
        return Result.ok(consumeService.save(consume));
    }


    @ApiOperation("4.查询还款记录")
    @GetMapping("paybacks")
    public Result<List<PayBack>> queryPayBack() {
        return Result.ok(payBackService.list(v -> v.orderByAsc(PayBack::getPayBackTime)));
    }

    @ApiOperation("5.查询还款总额")
    @GetMapping("paybacks/total")
    public Result<PaybackVo> queryPayBackTotal() {
        return Result.ok(payBackService.calculateTotalPayBack());
    }

    @ApiOperation("6.新增还款记录")
    @PostMapping("payback")
    public Result<Boolean> addPayBackRecord(@RequestBody PayBack payBack) {
        return Result.ok(payBackService.save(payBack));
    }

    @ApiOperation("7.新增退款记录")
    @PostMapping("payback/cancel")
    public Result<Boolean> cancelRecord(@RequestParam("id") String id) {
        consumeService.cancelRecord(id);
        return Result.ok();
    }

    @ApiOperation("8.新增提前还款记录")
    @PostMapping("payback/early")
    public Result<Boolean> addEarlyPaidRecord(@RequestParam("id") String id) {
        consumeService.earlyPaidRecord(id);
        return Result.ok();
    }

    @ApiOperation("9.计算本期应还款金额")
    @GetMapping("payback/calculate/current")
    public Result<PaybackVo> calculateCurrentPayBack() {
        return Result.ok(payBackService.calculateCurrentPayBack());
    }
}

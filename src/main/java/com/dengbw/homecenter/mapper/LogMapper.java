package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月14日 14:23
 */
@Mapper
public interface LogMapper extends BaseMapper<Log> {
}

package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.BomRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月11日 13:45
 */
@Mapper
public interface BomRelationMapper extends BaseMapper<BomRelation> {
}

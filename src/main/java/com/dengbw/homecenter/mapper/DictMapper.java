package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.Dict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年08月03日 13:32
 */
@Mapper
public interface DictMapper extends BaseMapper<Dict> {
}

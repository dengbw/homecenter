package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.Env;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月13日 14:34
 */
@Mapper
public interface EnvMapper extends BaseMapper<Env> {
}

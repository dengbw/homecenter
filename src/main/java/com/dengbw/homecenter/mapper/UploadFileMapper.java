package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.UploadFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月19日 16:16
 */
@Mapper
public interface UploadFileMapper extends BaseMapper<UploadFile> {
}

package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.BomItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月11日 10:49
 */
@Mapper
public interface BomItemMapper extends BaseMapper<BomItem> {
}

package com.dengbw.homecenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengbw.homecenter.model.db.PayBack;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dengbw
 * 2022年07月22日 09:30
 */
@Mapper
public interface PayBackMapper extends BaseMapper<PayBack> {
}

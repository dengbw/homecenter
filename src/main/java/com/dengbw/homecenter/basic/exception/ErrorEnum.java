package com.dengbw.homecenter.basic.exception;

import lombok.Getter;

/**
 * @author dengbw
 * 2022年08月15日 16:56
 */
@Getter
public enum ErrorEnum {
    SUCCESS(200, "成功"),
    INTERNAL_SERVER_ERROR(500, "服务器异常"),
    EMPTY_INPUT(1001, "内容为空"),
    EMPTY_KEY(1002, "字典-键为空"),
    EMPTY_VALUE(1003, "字典-值为空"),
    EMPTY_CATEGORY(1004, "字典-类别为空"),
    CREATE_DUPLICATE(2001, "字典新增重复值"),
    ;

    private final Integer errorCode;
    private final String errorMsg;

    ErrorEnum(Integer code, String msg) {
        errorCode = code;
        errorMsg = msg;
    }
}

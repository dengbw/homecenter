package com.dengbw.homecenter.basic.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年08月15日 16:54
 */
@Getter
@Setter
public class BizException extends RuntimeException{
    private final Integer errorCode;
    private final String errorMsg;

    public BizException(ErrorEnum errorEnum) {
        errorCode = errorEnum.getErrorCode();
        errorMsg = errorEnum.getErrorMsg();
    }
}

package com.dengbw.homecenter.basic.exception;

import com.dengbw.homecenter.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author dengbw
 * 2022年08月15日 16:58
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BizException.class)
    public Result<String> bizExceptionHandler(BizException e) {
        log.error(e.getErrorMsg());
        return Result.ofDefinedError(e);
    }

    @ExceptionHandler(Exception.class)
    public Result<String> exceptionHandler(Exception e) {
        log.error(e.getMessage());
        return Result.ofUndefinedError(ErrorEnum.INTERNAL_SERVER_ERROR);
    }
}

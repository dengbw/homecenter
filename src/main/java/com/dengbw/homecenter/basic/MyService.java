package com.dengbw.homecenter.basic;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @author dengbw
 * 2022年07月13日 15:32
 */
@SuppressWarnings("unused")
public class MyService<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    public List<T> list(MyAction<LambdaQueryWrapper<T>> action) {
        LambdaQueryWrapper<T> wrapper = new LambdaQueryWrapper<>();
        if (action != null) {
            action.apply(wrapper);
        }
        return list(wrapper);
    }

    public IPage<T> page(Page<T> page, MyAction<LambdaQueryWrapper<T>> action) {
        LambdaQueryWrapper<T> wrapper = new LambdaQueryWrapper<>();
        if (action != null) {
            action.apply(wrapper);
        }
        return page(page, wrapper);
    }

    public boolean update(MyAction<LambdaUpdateWrapper<T>> action) {
        LambdaUpdateWrapper<T> wrapper = new LambdaUpdateWrapper<>();
        if (action != null) {
            action.apply(wrapper);
        }
        return update(wrapper);
    }
}

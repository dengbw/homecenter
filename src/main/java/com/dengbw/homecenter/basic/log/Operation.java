package com.dengbw.homecenter.basic.log;

/**
 * @author dengbw
 * 2022年07月14日 13:58
 */
public enum Operation {
    /** 增 */
    ADD,
    /** 删 */
    REMOVE,
    /** 改 */
    EDIT,
    /** 查 */
    QUERY,
    /** 导入 */
    IMPORT,
    /** 导出 */
    REPORT,
    /** 上传 */
    UPLOAD
}

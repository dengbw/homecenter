package com.dengbw.homecenter.basic.log.aop;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.basic.exception.BizException;
import com.dengbw.homecenter.basic.log.BaseContext;
import com.dengbw.homecenter.basic.log.Operation;
import com.dengbw.homecenter.utils.ExceptionUtils;
import io.swagger.annotations.ApiOperation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import javax.annotation.Resource;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author dengbw
 * 2022年07月14日 15:56
 */
@Aspect
@Component
public class LogAspect {
    ThreadLocal<Long> currentTime = new ThreadLocal<>();

    /**
     * 基础上下文
     */
    @Resource
    private BaseContext context;

    /**
     * 配置切入点（切面编程）
     */
    @Pointcut("@annotation(io.swagger.annotations.ApiOperation)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     * 处理系统日志（配置环绕通知,使用在方法logPointcut()上注册的切入点）
     *
     * @param joinPoint join point for advice
     */
    @Around("logPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        // 记录方法的执行时间
        currentTime.set(System.currentTimeMillis());
        // 执 行 方 法
        result = joinPoint.proceed();
        // 注解解析
        ApiOperation annotation = getAnnotation(joinPoint);
        String entry = annotation.value();
        Operation action = getOperation(joinPoint);
        String describe = annotation.notes();
        // 获取方法名
        String methodName = getMethodName(joinPoint);
        // 获取参数
        String parameter = getParameterToJson(joinPoint);
        // 请求耗时
        Long time = System.currentTimeMillis() - currentTime.get();
        currentTime.remove();
        // 记录日志
        context.record(LocalDateTime.now(), entry, describe, methodName, parameter, time, action, null);
        return result;
    }

    /**
     * 配置异常通知
     *
     * @param joinPoint join point for advice
     * @param e         exception
     */
    @AfterThrowing(pointcut = "logPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) throws IOException {
        // 注解解析
        ApiOperation annotation = getAnnotation((ProceedingJoinPoint) joinPoint);
        String entry = annotation.value();
        Operation operation = getOperation((ProceedingJoinPoint) joinPoint);
        String describe = annotation.notes();
        // 获取方法名
        String methodName = getMethodName((ProceedingJoinPoint) joinPoint);
        // 获取参数
        String parameter = getParameterToJson((ProceedingJoinPoint) joinPoint);
        // 请求耗时
        Long timeUsed = System.currentTimeMillis() - currentTime.get();
        currentTime.remove();
        // 异常详细
        String exceptionDetail;
        if (e instanceof BizException be) {
            exceptionDetail = be.getErrorMsg();
        } else {
            exceptionDetail = ExceptionUtils.getMessage((Exception) e);
        }
        // 记录日志
        context.record(LocalDateTime.now(), entry, describe, methodName, parameter, timeUsed, operation, exceptionDetail);
    }

    /**
     * 获取注解
     */
    public ApiOperation getAnnotation(ProceedingJoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Class<?> targetClass = point.getTarget().getClass();
        ApiOperation targetLog = targetClass.getAnnotation(ApiOperation.class);
        if (targetLog != null) {
            return targetLog;
        } else {
            Method method = signature.getMethod();
            return method.getAnnotation(ApiOperation.class);
        }
    }

    public Operation getOperation(ProceedingJoinPoint point) {
        Class<?> targetClass = point.getTarget().getClass();
        Annotation[] annotations = targetClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof GetMapping) {
                return Operation.QUERY;
            } else if (annotation instanceof DeleteMapping) {
                return Operation.REMOVE;
            } else if (annotation instanceof PostMapping) {
                return Operation.EDIT;
            } else if (annotation instanceof PutMapping) {
                return Operation.ADD;
            }
        }
        return Operation.IMPORT;
    }

    /**
     * 获取方法名
     */
    public String getMethodName(ProceedingJoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        // 方法路径
        return point.getTarget().getClass().getName()+"."+signature.getName()+"()";
    }

    /**
     * 获取参数（转换json格式）
     */
    public String getParameterToJson(ProceedingJoinPoint point) {
        List<Object> argList = new ArrayList<>();
        //参数值
        Object[] argValues = point.getArgs();
        //参数名称
        String[] argNames = ((MethodSignature)point.getSignature()).getParameterNames();
        if(argValues != null){
            for (int i = 0; i < argValues.length; i++) {
                Map<String, Object> map = new HashMap<>();
                String key = argNames[i];
                map.put(key, argValues[i]);
                argList.add(map);
            }
        }
        if (CollectionUtils.isEmpty(argList)) {
            return "";
        }
        return argList.size() == 1 ? JSONUtil.toJsonStr(argList.get(0)) : JSONUtil.toJsonStr(argList);
    }
}

package com.dengbw.homecenter.basic.log;

import com.dengbw.homecenter.model.db.Log;
import com.dengbw.homecenter.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * @author dengbw
 * 2022年07月14日 14:02
 */
@Component
public class BaseContext {
    /**
     * 日志服务
     */
    @Autowired
    private LogService logService;

    /**
     * 新增日志
     */
    @Async
    @Transactional(rollbackFor = Exception.class)
    public void record(LocalDateTime operationTime,
                       String entry,
                       String describe,
                       String method,
                       String param,
                       Long timeUsed,
                       Operation type,
                       String error) {
        logService.save(new Log(operationTime, entry, describe, method, param, timeUsed, type, error));
    }
}

package com.dengbw.homecenter.basic;

/**
 * @author dengbw
 * 2022年07月13日 15:26
 */
@FunctionalInterface
public interface MyAction<T> {
    void apply(T t);
}

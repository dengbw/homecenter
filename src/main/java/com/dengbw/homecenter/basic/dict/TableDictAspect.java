package com.dengbw.homecenter.basic.dict;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author dengbw
 * 2022年09月13日 09:03
 */
@Aspect
@Component
public class TableDictAspect {
    @Pointcut("@annotation(TableDict)")
    public void pointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    @After("pointcut()")
    public void after(JoinPoint joinPoint) {
        Object target = joinPoint.getTarget();
    }
}

package com.dengbw.homecenter.model.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dengbw.homecenter.basic.log.Operation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Log {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private LocalDateTime operationTime;
    private String entry;
    private String describe;
    private String method;
    private String param;
    private Long timeUsed;
    private Operation type;
    private String error;

    public Log(LocalDateTime operationTime, String entry, String describe, String method, String param, Long timeUsed, Operation type, String error) {
        this.operationTime = operationTime;
        this.entry = entry;
        this.describe = describe;
        this.method = method;
        this.param = param;
        this.timeUsed = timeUsed;
        this.type = type;
        this.error = error;
    }
}

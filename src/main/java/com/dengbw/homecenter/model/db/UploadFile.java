package com.dengbw.homecenter.model.db;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dengbw
 * 2022年07月19日 16:14
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UploadFile {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String originName;
//    private String storageName;
    private String path;
    private String format;
    private Long size;
    private LocalDateTime createTime;
}

package com.dengbw.homecenter.model.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dengbw.homecenter.model.vo.BomExcel;
import com.dengbw.homecenter.utils.excel.ExcelImport;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BomItem {
  @TableId(type = IdType.ASSIGN_UUID)
  private String id;
  @ExcelImport("Comment")
  private String comment;
  @ExcelImport("Description")
  private String description;
  @ExcelImport("Footprint")
  private String footprint;
  @ExcelImport("LibRef")
  private String libRef;
  @ExcelImport("QuantityBought")
  private Long quantityRemained;

  private String image;

  public BomItem(BomExcel excel) {
    comment = excel.getComment();
    description = excel.getDescription();
    footprint = excel.getFootprint();
    libRef = excel.getLibRef();
    quantityRemained = excel.getQuantityBought();
  }
}

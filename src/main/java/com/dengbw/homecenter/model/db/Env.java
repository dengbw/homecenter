package com.dengbw.homecenter.model.db;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dengbw
 * 2022年07月13日 14:32
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Env {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String temperature;
    private String humidity;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}

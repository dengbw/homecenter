package com.dengbw.homecenter.model.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PayBack {

  @TableId(type = IdType.ASSIGN_UUID)
  private String id;
  // 本金
  @JsonProperty("本金(元)")
  private BigDecimal principal;
  // 利息
  @JsonProperty("利息(元)")
  private BigDecimal interest;
  @JsonProperty("合计(元)")
  private BigDecimal total;
  @JsonProperty("还款时间")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime payBackTime;
}

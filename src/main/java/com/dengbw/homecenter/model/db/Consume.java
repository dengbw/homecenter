package com.dengbw.homecenter.model.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dengbw.homecenter.basic.dict.TableDict;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Consume {
  @TableId(type = IdType.ASSIGN_UUID)
  private String id;
  @JsonProperty("金额(元)")
  private BigDecimal money;
  @JsonProperty("支付卡")
  private String source;
  @JsonProperty("收款方")
  private String target;
  @JsonProperty("消费时间")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime occurTime;
  @JsonProperty("消费描述")
  private String description;
  @JsonProperty("是否取消订单")
  private Boolean canceled;
  @JsonProperty("是否提前还款")
  private Boolean earlyPaid;
  @JsonProperty("类别")
  @TableDict("consume_category")
  private String category;
}

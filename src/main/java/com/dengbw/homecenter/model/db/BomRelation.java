package com.dengbw.homecenter.model.db;

import com.dengbw.homecenter.utils.excel.ExcelImport;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年07月11日 13:42
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BomRelation {
    private String bomId;
    private String bomItemId;
    @ExcelImport("QuantityRequired")
    private Long quantityRequired;
    @ExcelImport("Designator")
    private String designator;
}

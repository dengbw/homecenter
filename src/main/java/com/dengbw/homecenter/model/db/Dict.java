package com.dengbw.homecenter.model.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Dict {
  @TableId(type = IdType.ASSIGN_UUID)
  private String id;
  private String category;
  private String showing;
  private String value;
}

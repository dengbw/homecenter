package com.dengbw.homecenter.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年09月05日 15:21
 */
@Getter
@Setter
@AllArgsConstructor
public class PaybackVo {
    @JsonProperty("本金")
    private String principal;
    @JsonProperty("利息")
    private String interest;
    @JsonProperty("合计")
    private String total;

    public PaybackVo() {
        principal = "0";
        interest = "0";
        total = "0";
    }
}

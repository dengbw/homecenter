package com.dengbw.homecenter.model.vo;

import com.dengbw.homecenter.utils.excel.ExcelImport;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年07月11日 16:26
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BomExcel {
    @ExcelImport("Comment")
    private String comment;
    @ExcelImport("Description")
    private String description;
    @ExcelImport("Designator")
    private String designator;
    @ExcelImport("Footprint")
    private String footprint;
    @ExcelImport("LibRef")
    private String libRef;
    @ExcelImport("QuantityRequired")
    private long quantityRequired;
    @ExcelImport("QuantityBought")
    private long quantityBought;
}

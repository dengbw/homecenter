package com.dengbw.homecenter.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dengbw
 * 2022年07月11日 10:58
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BomVo {
    private String id;
    private String name;
    private String description;
    private List<BomItemVo> items;
}

package com.dengbw.homecenter.model.vo;

import com.dengbw.homecenter.model.db.BomItem;
import com.dengbw.homecenter.model.db.BomRelation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年07月11日 11:10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BomItemVo {
    private String id;
    private String comment;
    private String description;
    private String designator;
    private String footprint;
    private String libRef;
    private long quantityRequired;
    private long quantityRemained;

    public BomItemVo(BomItem item, BomRelation relation) {
        id = item.getId();
        comment = item.getComment();
        description = item.getDescription();
        designator = relation.getDesignator();
        footprint = item.getFootprint();
        libRef = item.getLibRef();
        quantityRemained = item.getQuantityRemained();
        quantityRequired = relation.getQuantityRequired();
    }
}

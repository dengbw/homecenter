package com.dengbw.homecenter.model.vo;

import com.dengbw.homecenter.model.db.Dict;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年08月03日 13:35
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DictItemVo {
    private String id;
    private String value;
    private String showing;

    public DictItemVo(Dict dict) {
        value = dict.getValue();
        showing = dict.getShowing();
    }
}

package com.dengbw.homecenter.model.vo;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.dengbw.homecenter.model.db.Dict;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dengbw
 * 2022年08月03日 13:33
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DictVo {
    private String category;
    private List<DictItemVo> values;

    public DictVo(List<Dict> dictList) {
        if (CollectionUtils.isNotEmpty(dictList)) {
            category = dictList.get(0).getCategory();
            values = dictList.stream().map(DictItemVo::new).toList();
        }
    }
}

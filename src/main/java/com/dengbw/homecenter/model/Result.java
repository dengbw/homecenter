package com.dengbw.homecenter.model;

import com.dengbw.homecenter.basic.exception.BizException;
import com.dengbw.homecenter.basic.exception.ErrorEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dengbw
 * 2022年07月11日 10:16
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Result<T> {

    private Integer code;
    private String msg;
    private Boolean success;
    private T data;

    public static <T> Result<T> ok() {
        return new Result<>(200, null, true, null);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<>(200, null, true, data);
    }

    public static <T> Result<T> failed(String msg, T data) {
        return new Result<>(-1, msg, false, data);
    }

    public static <T> Result<T> ofDefinedError(BizException be) {
        return new Result<>(be.getErrorCode(), be.getErrorMsg(), false, null);
    }

    public static <T> Result<T> ofUndefinedError(ErrorEnum errorEnum) {
        return new Result<>(errorEnum.getErrorCode(), errorEnum.getErrorMsg(), false, null);
    }
}

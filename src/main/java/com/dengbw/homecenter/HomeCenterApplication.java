package com.dengbw.homecenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeCenterApplication.class, args);
    }

}

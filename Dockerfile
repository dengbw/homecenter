FROM openjdk:17-alpine3.14
RUN apk update && apk add --no-cache ca-certificates

# 设置alpine 时间为上海时间
RUN apk add tzdata && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone \
    && apk del tzdata
WORKDIR /root/
RUN mkdir config
COPY target/HomeCenter-0.0.1-SNAPSHOT.jar .
COPY src/main/resources/application.yml config/
COPY src/main/resources/application-dev.yml config/

EXPOSE 9999

CMD ["java", "-jar", "HomeCenter-0.0.1-SNAPSHOT.jar"]
